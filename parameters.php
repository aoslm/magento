<?php
require 'app/bootstrap.php';
require 'app/Mage.php';
try {
    $app = Mage::app('default');
    $installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
    $installer->startSetup();
  // Add new attribute 29776_colour_of_product
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, '29776_colour_of_product', array(
    'label'                      => 'Colour of product',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The colour e.g. red, blue, green, black, white.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10674_design',
        '29776_colour_of_product',
        100
  );

  // Add new attribute 101037_product_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, '101037_product_type', array(
    'label'                      => 'Product type',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The sub-category of the product.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10674_design',
        '101037_product_type',
        100
  );

  // Add new attribute a4692_form_factor
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a4692_form_factor', array(
    'label'                      => 'Form factor',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The shape or design of the product.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10674_design',
        'a4692_form_factor',
        100
  );

  // Add new attribute a46290_housing_material
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a46290_housing_material', array(
    'label'                      => 'Housing material',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10674_design',
        'a46290_housing_material',
        100
  );

  // Add new attribute a2787_display_diagonal
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2787_display_diagonal', array(
    'label'                      => 'Display diagonal',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Size of the display for this product, measured diagonally, usually in inches.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a2787_display_diagonal',
        100
  );

  // Add new attribute a9350_display_resolution
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a9350_display_resolution', array(
    'label'                      => 'Display resolution',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '"The number of distinct pixels in each dimension that can be displayed. It is usually quoted as width × height, with the units in pixels: for example, 1024 × 768"" means the width is 1024 pixels and the height is 768 pixels. The higher the number of pixels, the sharper the image."""',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a9350_display_resolution',
        100
  );

  // Add new attribute a36303_touchscreen
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a36303_touchscreen', array(
    'label'                      => 'Touchscreen',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'A screen of a laptop, smartphone, etc., that responds to touch so you can control the interface using your fingers.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a36303_touchscreen',
        100
  );

  // Add new attribute a36493_led_backlight
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a36493_led_backlight', array(
    'label'                      => 'LED backlight',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'A backlight is a form of illumination used in liquid crystal displays (LCDs). One type of backlight is made by LEDs, which can be either white or RGB (red green blue). White LEDs are used most often in notebooks and desktop screens, and in virtually all mobile LCD screens. RGB LEDs for backlighting are found in high-end color proofing displays such as HP DreamColor LP2480zx monitor or selected HP EliteBook notebooks, as well as newer consumer grade displays such as Dell\'s Studio series laptops which have an optional RGB LED display.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a36493_led_backlight',
        100
  );

  // Add new attribute a149038_hd_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a149038_hd_type', array(
    'label'                      => 'HD type',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Type of supported High Definition (e.g. Full HD, 4K Ultra HD).',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a149038_hd_type',
        100
  );

  // Add new attribute a45324_3d
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a45324_3d', array(
    'label'                      => '3D',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The device can be used with 3D technology.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a45324_3d',
        100
  );

  // Add new attribute a16353_aspect_ratio
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a16353_aspect_ratio', array(
    'label'                      => 'Aspect ratio',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The aspect ratio is the ratio of the width of a shape to its height. For example, 4:3 is common for standard displays and 16:9 is the ratio for Widescreen TV.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a16353_aspect_ratio',
        100
  );

  // Add new attribute a12514_display_brightness
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12514_display_brightness', array(
    'label'                      => 'Display brightness',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Brightness is the amount of radiating light emitted from the screen. The brightness rating is measured in candellas per square meter, commonly known as \'nits\'.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a12514_display_brightness',
        100
  );

  // Add new attribute a91135_panel_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a91135_panel_type', array(
    'label'                      => 'Panel type',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a91135_panel_type',
        100
  );

  // Add new attribute a73715_processor_frequency
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a73715_processor_frequency', array(
    'label'                      => 'Processor frequency',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The speed that the microprocessor executes each instruction or each vibration of the clock. The CPU requires a fixed number of clock ticks, or cycles, to execute each instruction. The faster the clocks rate, the faster the CPU, or the faster it can execute instructions. Clock Speeds are usually determined in MHz, 1 MHz representing 1 million cycles per second, or in GHz, 1 GHz representing 1 thousand million cycles per second. The higher the CPU speed, the better a computer will perform.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a73715_processor_frequency',
        100
  );

  // Add new attribute a49_processor_model
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a49_processor_model', array(
    'label'                      => 'Processor model',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The model number for the processor in a computer.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a49_processor_model',
        100
  );

  // Add new attribute a11560_processor_family
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a11560_processor_family', array(
    'label'                      => 'Processor family',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'A family of processors is a group of processors produced by one company over a short period of time e.g. Intel Pentium processors.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a11560_processor_family',
        100
  );

  // Add new attribute a56907_processor_boost_frequen
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a56907_processor_boost_frequen', array(
    'label'                      => 'Processor boost frequency',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The turbo boost is an automatic, managed accelleration of the processor when one of the cores is overloaded.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a56907_processor_boost_frequen',
        100
  );

  // Add new attribute a57809_processor_cores
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57809_processor_cores', array(
    'label'                      => 'Processor cores',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The number of central processing units (\'cores\') in a processor. Some processors have 1 core, others have 2 (e.g. Intel Core Duo) or more (e.g. the Intel Xeon E7-2850 has 10 cores).',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a57809_processor_cores',
        100
  );

  // Add new attribute a71259_processor_threads
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a71259_processor_threads', array(
    'label'                      => 'Processor threads',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a71259_processor_threads',
        100
  );

  // Add new attribute a39188_system_bus_rate
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a39188_system_bus_rate', array(
    'label'                      => 'System bus rate',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'A bus is a communication system that transfers data between components inside a computer, or between computers. The system bus rate is the speed at which data is transferred in this communication system.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a39188_system_bus_rate',
        100
  );

  // Add new attribute a95853_processor_cache
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a95853_processor_cache', array(
    'label'                      => 'Processor cache',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a95853_processor_cache',
        100
  );

  // Add new attribute a135207_processor_cache_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135207_processor_cache_type', array(
    'label'                      => 'Processor cache type',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a135207_processor_cache_type',
        100
  );

  // Add new attribute a31183_processor_socket
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a31183_processor_socket', array(
    'label'                      => 'Processor socket',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Mechanical component(s) that provides mechanical and electrical connections between a microprocessor and a printed circuit board (PCB). This allows the processor to be replaced without soldering.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a31183_processor_socket',
        100
  );

  // Add new attribute a47168_motherboard_chipset
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a47168_motherboard_chipset', array(
    'label'                      => 'Motherboard chipset',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The chipset connects the microprocessor to the rest of the motherboard.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a47168_motherboard_chipset',
        100
  );

  // Add new attribute a140084_integrated_4g_wimax
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a140084_integrated_4g_wimax', array(
    'label'                      => 'Integrated 4G WiMAX',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a140084_integrated_4g_wimax',
        100
  );

  // Add new attribute a135200_scenario_design_power_
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135200_scenario_design_power_', array(
    'label'                      => 'Scenario Design Power (SDP)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a135200_scenario_design_power_',
        100
  );

  // Add new attribute a133010_processor_series
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133010_processor_series', array(
    'label'                      => 'Processor series',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a133010_processor_series',
        100
  );

  // Add new attribute a132803_stepping
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132803_stepping', array(
    'label'                      => 'Stepping',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a132803_stepping',
        100
  );

  // Add new attribute a133027_maximum_number_of_pci_
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133027_maximum_number_of_pci_', array(
    'label'                      => 'Maximum number of PCI Express lanes',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a133027_maximum_number_of_pci_',
        100
  );

  // Add new attribute a132792_processor_lithography
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132792_processor_lithography', array(
    'label'                      => 'Processor lithography',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The process which is performed by the processor e.g. CPU (Central Processing Unit).',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a132792_processor_lithography',
        100
  );

  // Add new attribute a133030_pci_express_slots_vers
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133030_pci_express_slots_vers', array(
    'label'                      => 'PCI Express slots version',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a133030_pci_express_slots_vers',
        100
  );

  // Add new attribute a132798_processor_operating_mo
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132798_processor_operating_mo', array(
    'label'                      => 'Processor operating modes',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Operating modes for the processors that place restrictions on the type and scope of operations for certain processes run by the CPU. This design allows the operating system to run with more privileges than application software.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a132798_processor_operating_mo',
        100
  );

  // Add new attribute a132815_processor_codename
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132815_processor_codename', array(
    'label'                      => 'Processor codename',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a132815_processor_codename',
        100
  );

  // Add new attribute a133078_tjunction
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133078_tjunction', array(
    'label'                      => 'Tjunction',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a133078_tjunction',
        100
  );

  // Add new attribute a133007_thermal_design_power_t
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133007_thermal_design_power_t', array(
    'label'                      => 'Thermal Design Power (TDP)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a133007_thermal_design_power_t',
        100
  );

  // Add new attribute a133031_pci_express_configurat
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133031_pci_express_configurat', array(
    'label'                      => 'PCI Express configurations',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a133031_pci_express_configurat',
        100
  );

  // Add new attribute a135138_conflict_free_processo
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135138_conflict_free_processo', array(
    'label'                      => 'Conflict Free processor',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '"Conflict free"" means ""DRC conflict free"", which is defined by the U.S. Securities and Exchange Commission rules to mean products that do not contain conflict minerals (tin, tantalum, tungsten and/or gold) that directly or indirectly finance or benefit armed groups in the Democratic Republic of the Congo (DRC) or adjoining countries."""',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a135138_conflict_free_processo',
        100
  );

  // Add new attribute a132882_ecc_supported_by_proce
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132882_ecc_supported_by_proce', array(
    'label'                      => 'ECC supported by processor',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a132882_ecc_supported_by_proce',
        100
  );

  // Add new attribute a73753_internal_memory
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a73753_internal_memory', array(
    'label'                      => 'Internal memory',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'A computer\'s memory which is directly accessible to the CPU.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g106_memory',
        'a73753_internal_memory',
        100
  );

  // Add new attribute a12693_internal_memory_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12693_internal_memory_type', array(
    'label'                      => 'Internal memory type',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The type of internal memory such as RAM, GDDR5.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g106_memory',
        'a12693_internal_memory_type',
        100
  );

  // Add new attribute a76682_memory_form_factor
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a76682_memory_form_factor', array(
    'label'                      => 'Memory form factor',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Design of the memory e.g. 240-pin DIMM, SO-DIMM.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g106_memory',
        'a76682_memory_form_factor',
        100
  );

  // Add new attribute a25759_memory_clock_speed
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a25759_memory_clock_speed', array(
    'label'                      => 'Memory clock speed',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The frequency at which the memory (e.g. RAM) runs.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g106_memory',
        'a25759_memory_clock_speed',
        100
  );

  // Add new attribute a10354_maximum_internal_memory
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a10354_maximum_internal_memory', array(
    'label'                      => 'Maximum internal memory',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The maximum internal memory which is available in the product.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g106_memory',
        'a10354_maximum_internal_memory',
        100
  );

  // Add new attribute a30921_total_storage_capacity
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a30921_total_storage_capacity', array(
    'label'                      => 'Total storage capacity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The total amount of data that can be stored on the device.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a30921_total_storage_capacity',
        100
  );

  // Add new attribute a74320_storage_media
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a74320_storage_media', array(
    'label'                      => 'Storage media',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Used for the storage of date e.g. HDD (hard disk drive), SSD (solid-state drive).',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a74320_storage_media',
        100
  );

  // Add new attribute a30920_number_of_hard_drives_i
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a30920_number_of_hard_drives_i', array(
    'label'                      => 'Number of hard drives installed',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The number of hard drives built into the device.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a30920_number_of_hard_drives_i',
        100
  );

  // Add new attribute a2705_hard_drive_interface
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2705_hard_drive_interface', array(
    'label'                      => 'Hard drive interface',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The way that a hard disk drive (HDD) is connected to the rest of the computer through a \'bus\' such as ATA or SCSI.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a2705_hard_drive_interface',
        100
  );

  // Add new attribute a1198_hard_drive_speed
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a1198_hard_drive_speed', array(
    'label'                      => 'Hard drive speed',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The rotational speed of a hard disk expressed in rotations per minute. The faster the better.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a1198_hard_drive_speed',
        100
  );

  // Add new attribute a74322_hybrid_hard_drive_hhdd_
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a74322_hybrid_hard_drive_hhdd_', array(
    'label'                      => 'Hybrid hard drive (H-HDD) capacity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a74322_hybrid_hard_drive_hhdd_',
        100
  );

  // Add new attribute a74323_hybrid_hard_drive_hhdd_
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a74323_hybrid_hard_drive_hhdd_', array(
    'label'                      => 'Hybrid hard drive (H-HDD) cache memory',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a74323_hybrid_hard_drive_hhdd_',
        100
  );

  // Add new attribute a20725_hard_drive_size
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a20725_hard_drive_size', array(
    'label'                      => 'Hard drive size',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a20725_hard_drive_size',
        100
  );

  // Add new attribute a19149_card_reader_integrated
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a19149_card_reader_integrated', array(
    'label'                      => 'Card reader integrated',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The device includes a card reader e.g. a card reader in the disk bay of a computer or thin client to permit log-on using smart/pin cards.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a19149_card_reader_integrated',
        100
  );

  // Add new attribute a9470_compatible_memory_cards
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a9470_compatible_memory_cards', array(
    'label'                      => 'Compatible memory cards',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Types of memory cards which can be used with this product.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a9470_compatible_memory_cards',
        100
  );

  // Add new attribute a74327_onboard_graphics_adapte
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a74327_onboard_graphics_adapte', array(
    'label'                      => 'On-board graphics adapter',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Graphics hardware which is built into the motherboard or CPU, as opposed to a separate graphics adapter (video card). On-board graphics uses CPU and RAM for graphics processing.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a74327_onboard_graphics_adapte',
        100
  );

  // Add new attribute a178874_discrete_graphics_adap
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a178874_discrete_graphics_adap', array(
    'label'                      => 'Discrete graphics adapter',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a178874_discrete_graphics_adap',
        100
  );

  // Add new attribute a192527_onboard_graphics_adapt
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a192527_onboard_graphics_adapt', array(
    'label'                      => 'On-board graphics adapter family',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a192527_onboard_graphics_adapt',
        100
  );

  // Add new attribute a53952_onboard_graphics_adapte
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a53952_onboard_graphics_adapte', array(
    'label'                      => 'On-board graphics adapter model',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '"Graphic adapter"" is the hardware that produces images on a display. ""On-board"" means that the graphics adapter is intergrated inside the motherboard."""',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a53952_onboard_graphics_adapte',
        100
  );

  // Add new attribute a133016_number_of_displays_sup
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133016_number_of_displays_sup', array(
    'label'                      => 'Number of displays supported (on-board graphics)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The feature may not be available on all computing systems. Functionality, performance, and other benefits of this feature may vary depending on system configuration.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a133016_number_of_displays_sup',
        100
  );

  // Add new attribute a132904_onboard_graphics_adapt
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132904_onboard_graphics_adapt', array(
    'label'                      => 'On-board graphics adapter base frequency',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a132904_onboard_graphics_adapt',
        100
  );

  // Add new attribute a132909_onboard_graphics_adapt
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132909_onboard_graphics_adapt', array(
    'label'                      => 'On-board graphics adapter burst frequency',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a132909_onboard_graphics_adapt',
        100
  );

  // Add new attribute a132917_maximum_onboard_graphi
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132917_maximum_onboard_graphi', array(
    'label'                      => 'Maximum on-board graphics adapter memory',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a132917_maximum_onboard_graphi',
        100
  );

  // Add new attribute a53948_discrete_graphics_adapt
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a53948_discrete_graphics_adapt', array(
    'label'                      => 'Discrete graphics adapter model',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'A graphics adapter (often known as a video card) generates images for a display. A discrete graphics adapter plugs into the motherboard, and usually produces much better graphics than an integrated graphics adapter. There are various models of discrete graphics adapters.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a53948_discrete_graphics_adapt',
        100
  );

  // Add new attribute a2690_graphics_adapter_open_gl
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2690_graphics_adapter_open_gl', array(
    'label'                      => 'Graphics adapter Open GL support',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a2690_graphics_adapter_open_gl',
        100
  );

  // Add new attribute a132948_number_of_execution_un
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132948_number_of_execution_un', array(
    'label'                      => 'Number of execution units',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a132948_number_of_execution_un',
        100
  );

  // Add new attribute a12917_number_of_builtin_speak
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12917_number_of_builtin_speak', array(
    'label'                      => 'Number of built-in speakers',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g110_audio',
        'a12917_number_of_builtin_speak',
        100
  );

  // Add new attribute a26586_speaker_power
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a26586_speaker_power', array(
    'label'                      => 'Speaker power',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g110_audio',
        'a26586_speaker_power',
        100
  );

  // Add new attribute a2731_builtin_microphone
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2731_builtin_microphone', array(
    'label'                      => 'Built-in microphone',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Microphone that is found inside the product.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g110_audio',
        'a2731_builtin_microphone',
        100
  );

  // Add new attribute a91102_front_camera
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a91102_front_camera', array(
    'label'                      => 'Front camera',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Camera at the front of the product.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g3024_camera',
        'a91102_front_camera',
        100
  );

  // Add new attribute a91103_front_camera_resolution
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a91103_front_camera_resolution', array(
    'label'                      => 'Front camera resolution (numeric)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g3024_camera',
        'a91103_front_camera_resolution',
        100
  );

  // Add new attribute a2828_optical_drive_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2828_optical_drive_type', array(
    'label'                      => 'Optical drive type',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'An optical drive uses laser to read optical discs such as CDs, DVDs and Blu-Ray. Some types of optical drive are: CD ROM drive, CR-RW (CD writer) drive, DVD-ROM.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g32_optical_drive',
        'a2828_optical_drive_type',
        100
  );

  // Add new attribute a12480_lightscribe
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12480_lightscribe', array(
    'label'                      => 'LightScribe',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Optical disc recording technology, created by Hewlett-Packard (HP), that uses specially coated recordable CD and DVD media to produce laser-etched labels with text or graphics, as opposed to stick-on labels and printable discs.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g32_optical_drive',
        'a12480_lightscribe',
        100
  );

  // Add new attribute a41678_wifi
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a41678_wifi', array(
    'label'                      => 'Wi-Fi',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Popular technology that allows an electronic device to exchange data or connect to the internet wirelessly using radio waves.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a41678_wifi',
        100
  );

  // Add new attribute a1193_wifi_standards
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a1193_wifi_standards', array(
    'label'                      => 'Wi-Fi standards',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The type of wireless local area network (LAN). It can be ad-hoc, where units in a network communicate peer-to-peer, or Infrastructure, where units communicate with each other via an access point  A LAN interconnects computers in a small area e.g. home, school or office.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a1193_wifi_standards',
        100
  );

  // Add new attribute a43547_ethernet_lan
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a43547_ethernet_lan', array(
    'label'                      => 'Ethernet LAN',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'An Ethernet LAN (Local Area Network) interface is present, for a wired conection via a cable.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a43547_ethernet_lan',
        100
  );

  // Add new attribute a57701_ethernet_lan_data_rates
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57701_ethernet_lan_data_rates', array(
    'label'                      => 'Ethernet LAN data rates',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The different speed levels of the Ethernet LAN connection.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a57701_ethernet_lan_data_rates',
        100
  );

  // Add new attribute a18579_bluetooth
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a18579_bluetooth', array(
    'label'                      => 'Bluetooth',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Bluetooth is a low-power radio technology developed to replace the cables and wires currently used to link or connect electronic devices such as personal computers, printers, and a wide variety of handheld devices including mobile phones. Because it uses radio-wave connectivity, a Bluetooth-enabled device has a constant, established connection to whatever browser it uses. This saves the user the trouble of logging on to check for emails or news updates, for example.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a18579_bluetooth',
        100
  );

  // Add new attribute a34457_3g
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a34457_3g', array(
    'label'                      => '3G',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Third generation of mobile telecommunications technology, 3G finds application in wireless voice telephony, mobile Internet access, fixed wireless Internet access, video calls and mobile TV.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a34457_3g',
        100
  );

  // Add new attribute a95633_4g
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a95633_4g', array(
    'label'                      => '4G',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Fourth generation of mobile phone mobile communication technology standards. A 4G system provides mobile ultra-broadband Internet access, for example to laptops with USB wireless modems, to smartphones, and to other mobile devices. Conceivable applications include amended mobile web access, IP telephony, gaming services, high-definition mobile TV, video conferencing, 3D television, and cloud computing.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a95633_4g',
        100
  );

  // Add new attribute a12819_usb_2.0_ports_quantity
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12819_usb_2.0_ports_quantity', array(
    'label'                      => 'USB 2.0 ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '"Number of USB 2.0 ports (connecting interfaces) in the device. USB 2.0 was released in April 2000 (now called Hi-Speed""), adding higher maximum signaling rate of 480 Mbit/s (effective throughput up to 35 MB/s or 280 Mbit/s), USB 2.0 ports are usually black."""',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a12819_usb_2.0_ports_quantity',
        100
  );

  // Add new attribute a138249_usb_3.0_3.1_gen_1_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a138249_usb_3.0_3.1_gen_1_type', array(
    'label'                      => 'USB 3.0 (3.1 Gen 1) Type-A ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a138249_usb_3.0_3.1_gen_1_type',
        100
  );

  // Add new attribute a38729_esata/usb_2.0_ports_qua
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a38729_esata/usb_2.0_ports_qua', array(
    'label'                      => 'eSATA/USB 2.0 ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The nunber of eSATA/USB 2.0 ports (connection sockets).',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a38729_esata/usb_2.0_ports_qua',
        100
  );

  // Add new attribute a46109_esata/usb_3.0_ports_qua
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a46109_esata/usb_3.0_ports_qua', array(
    'label'                      => 'eSATA/USB 3.0 ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a46109_esata/usb_3.0_ports_qua',
        100
  );

  // Add new attribute a12844_vga_dsub_ports_quantity
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12844_vga_dsub_ports_quantity', array(
    'label'                      => 'VGA (D-Sub) ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Number of VGA (D-Sub) ports (connecting interfaces) in the device. The VGA (D-Sub) connector is a 15 pin connector between a computer and a monitor. It was first introduced in 1987 by IBM.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a12844_vga_dsub_ports_quantity',
        100
  );

  // Add new attribute a26456_hdmi_ports_quantity
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a26456_hdmi_ports_quantity', array(
    'label'                      => 'HDMI ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '"The number of sockets (ports) for HDMI connections. HDMI (High-Definition Multimedia Interface) is a compact audio/video interface for transferring uncompressed video data and compressed/uncompressed digital audio data from a HDMI-compliant device (the source device"") to a compatible computer monitor, video projector, digital television, or digital audio device. HDMI is a digital replacement for existing analog video standards."""',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a26456_hdmi_ports_quantity',
        100
  );

  // Add new attribute a35644_displayports_quantity
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a35644_displayports_quantity', array(
    'label'                      => 'DisplayPorts quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Number of DisplayPorts. A DisplayPort is a digital display interface developed by the Video Electronics Standards Association (VESA). The interface is primarily used to connect a video source to a display device such as a computer monitor, though it can also be used to carry audio, USB, and other forms of data.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a35644_displayports_quantity',
        100
  );

  // Add new attribute a12837_dvi_port
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12837_dvi_port', array(
    'label'                      => 'DVI port',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Digital Visual Interface (DVI) is a video display interface to connect a video source to a display device, such as a computer monitor.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a12837_dvi_port',
        100
  );

  // Add new attribute a12827_ethernet_lan_rj45_ports
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12827_ethernet_lan_rj45_ports', array(
    'label'                      => 'Ethernet LAN (RJ-45) ports',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Number of Ethernet LAN (RJ-45) ports (connecting interfaces) in the device.  Ethernet LAN (RJ-45) ports allow a computer to connect to the ethernet.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a12827_ethernet_lan_rj45_ports',
        100
  );

  // Add new attribute a46289_thunderbolt_ports_quant
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a46289_thunderbolt_ports_quant', array(
    'label'                      => 'Thunderbolt ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The number of ports (sockets) for Thunderbolt connections. Thunderbolt is a hardware interface that allows for the connection of external peripherals to a computer. It uses the same connector as Mini DisplayPort (MDP). It was released in 2011 and is used in Apple\'s 2011 MacBook Pro, as well as other devices.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a46289_thunderbolt_ports_quant',
        100
  );

  // Add new attribute a95559_thunderbolt_2_ports_qua
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a95559_thunderbolt_2_ports_qua', array(
    'label'                      => 'Thunderbolt 2 ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a95559_thunderbolt_2_ports_qua',
        100
  );

  // Add new attribute a138392_thunderbolt_3_ports_qu
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a138392_thunderbolt_3_ports_qu', array(
    'label'                      => 'Thunderbolt 3 ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a138392_thunderbolt_3_ports_qu',
        100
  );

  // Add new attribute a12828_headphone_outputs
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12828_headphone_outputs', array(
    'label'                      => 'Headphone outputs',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Number of sockets /ports where headphones are connected.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a12828_headphone_outputs',
        100
  );

  // Add new attribute a12829_microphone_in
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12829_microphone_in', array(
    'label'                      => 'Microphone in',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The socket where a microphone is connected to the device.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a12829_microphone_in',
        100
  );

  // Add new attribute a59774_combo_headphone/mic_por
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a59774_combo_headphone/mic_por', array(
    'label'                      => 'Combo headphone/mic port',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a59774_combo_headphone/mic_por',
        100
  );

  // Add new attribute a42480_docking_connector
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a42480_docking_connector', array(
    'label'                      => 'Docking connector',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Connector used to attach a mobile electronic device simultaneously to multiple external resources. The dock connector will typically carry a variety of signals and power, through a single connector, to simplify the process of docking the mobile device. A dock connector may be embedded in a mechanical fixture used to support or align the mobile device or may be at the end of a cable.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a42480_docking_connector',
        100
  );

  // Add new attribute a12831_dcin_jack
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12831_dcin_jack', array(
    'label'                      => 'DC-in jack',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The socket/plug where the DC electricity supply connects to the device.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a12831_dcin_jack',
        100
  );

  // Add new attribute a77391_mini_displayport/thunde
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a77391_mini_displayport/thunde', array(
    'label'                      => 'Mini DisplayPort/Thunderbolt combo port',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a77391_mini_displayport/thunde',
        100
  );

  // Add new attribute a76730_lan/vga_combo_port
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a76730_lan/vga_combo_port', array(
    'label'                      => 'LAN/VGA combo port',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a76730_lan/vga_combo_port',
        100
  );

  // Add new attribute a208048_tv_out
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a208048_tv_out', array(
    'label'                      => 'TV out',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a208048_tv_out',
        100
  );

  // Add new attribute a139998_total_number_of_sata_c
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a139998_total_number_of_sata_c', array(
    'label'                      => 'Total number of SATA connectors',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a139998_total_number_of_sata_c',
        100
  );

  // Add new attribute a139999_number_of_sata_iii_con
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a139999_number_of_sata_iii_con', array(
    'label'                      => 'Number of SATA III connectors',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Serial ATA (Advanced Technology Attachment) (SATA) is a computer bus interface that connects host bus adapters to mass storage devices such as hard disk drives and optical drives. SATA III (revision 3.x) interface, formally known as SATA 6Gb/s, is a third generation SATA interface running at 6.0Gb/s. The bandwidth throughput, which is supported by the interface, is up to 600MB/s. This interface is backwards compatible with SATA 3 Gb/s interface.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a139999_number_of_sata_iii_con',
        100
  );

  // Add new attribute a1201_pointing_device
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a1201_pointing_device', array(
    'label'                      => 'Pointing device',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g762_keyboard',
        'a1201_pointing_device',
        100
  );

  // Add new attribute a2718_numeric_keypad
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2718_numeric_keypad', array(
    'label'                      => 'Numeric keypad',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '"Small, palm-sized, seventeen key section of a computer keyboard, usually on the very far right. The numeric keypad features digits 0 to 9, addition (+), subtraction (-), multiplication (*) and division (/) symbols, a decimal point (.) and Num Lock and Enter keys. Laptop keyboards often do not have a numpad, but may provide numpad input by holding a modifier key (typically labelled Fn"") and operating keys on the standard keyboard."""',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g762_keyboard',
        'a2718_numeric_keypad',
        100
  );

  // Add new attribute a2713_windows_keys
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2713_windows_keys', array(
    'label'                      => 'Windows keys',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g762_keyboard',
        'a2713_windows_keys',
        100
  );

  // Add new attribute a12350_operating_system_instal
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12350_operating_system_instal', array(
    'label'                      => 'Operating system installed',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Type of operating system on a device e.g. IOS on Apple devices, Android for mobile devices.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2933_software',
        'a12350_operating_system_instal',
        100
  );

  // Add new attribute a45085_operating_system_archit
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a45085_operating_system_archit', array(
    'label'                      => 'Operating system architecture',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The way in which the operating system of a computer/smartphone (32-bit/64-bit) is set up.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2933_software',
        'a45085_operating_system_archit',
        100
  );

  // Add new attribute a74045_intel_wireless_display_
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a74045_intel_wireless_display_', array(
    'label'                      => 'Intel® Wireless Display (Intel® WiDi)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Connect to TV Wirelessly',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a74045_intel_wireless_display_',
        100
  );

  // Add new attribute a132845_intel_stable_image_pla
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132845_intel_stable_image_pla', array(
    'label'                      => 'Intel Stable Image Platform Program (SIPP)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132845_intel_stable_image_pla',
        100
  );

  // Add new attribute a132847_intel_os_guard
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132847_intel_os_guard', array(
    'label'                      => 'Intel® OS Guard',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Protect Your Operating Environment',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132847_intel_os_guard',
        100
  );

  // Add new attribute a133019_intel_64
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133019_intel_64', array(
    'label'                      => 'Intel 64',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a133019_intel_64',
        100
  );

  // Add new attribute a132841_intel_secure_key
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132841_intel_secure_key', array(
    'label'                      => 'Intel® Secure Key',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Generate Cryptographic Security Protocols',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132841_intel_secure_key',
        100
  );

  // Add new attribute a132831_intel_trusted_executio
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132831_intel_trusted_executio', array(
    'label'                      => 'Intel Trusted Execution Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132831_intel_trusted_executio',
        100
  );

  // Add new attribute a133085_intel_virtualization_t
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133085_intel_virtualization_t', array(
    'label'                      => 'Intel Virtualization Technology for Directed I/O (VT-d)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Simplify Virtualization and Reduce Overheads',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a133085_intel_virtualization_t',
        100
  );

  // Add new attribute a132829_intel_aes_new_instruct
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132829_intel_aes_new_instruct', array(
    'label'                      => 'Intel® AES New Instructions (Intel® AES-NI)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Added Security with Faster Data Encryption',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132829_intel_aes_new_instruct',
        100
  );

  // Add new attribute a133021_execute_disable_bit
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133021_execute_disable_bit', array(
    'label'                      => 'Execute Disable Bit',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a133021_execute_disable_bit',
        100
  );

  // Add new attribute a132836_intel_vtx_with_extende
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132836_intel_vtx_with_extende', array(
    'label'                      => 'Intel VT-x with Extended Page Tables (EPT)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132836_intel_vtx_with_extende',
        100
  );

  // Add new attribute a133023_idle_states
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133023_idle_states', array(
    'label'                      => 'Idle States',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a133023_idle_states',
        100
  );

  // Add new attribute a135184_intel_rapid_storage_te
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135184_intel_rapid_storage_te', array(
    'label'                      => 'Intel Rapid Storage Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a135184_intel_rapid_storage_te',
        100
  );

  // Add new attribute a132833_intel_enhanced_halt_st
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132833_intel_enhanced_halt_st', array(
    'label'                      => 'Intel Enhanced Halt State',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132833_intel_enhanced_halt_st',
        100
  );

  // Add new attribute a132852_intel_clear_video_tech
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132852_intel_clear_video_tech', array(
    'label'                      => 'Intel® Clear Video Technology for Mobile Internet Devices (Intel CVT for MID)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132852_intel_clear_video_tech',
        100
  );

  // Add new attribute a176704_processor_ark_id
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a176704_processor_ark_id', array(
    'label'                      => 'Processor ARK ID',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a176704_processor_ark_id',
        100
  );

  // Add new attribute a135128_intel_virtualization_t
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135128_intel_virtualization_t', array(
    'label'                      => 'Intel Virtualization Technology (VT-x)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Simplify Virtualization and Reduce Overheads',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a135128_intel_virtualization_t',
        100
  );

  // Add new attribute a134926_intel_secure_boot
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a134926_intel_secure_boot', array(
    'label'                      => 'Intel Secure Boot',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Secure Boot ensures that only trusted software with a known configuration executes as part of the boot process.  It enables the hardware root of trust which starts the authentication chain for platform firmware and subsequent software load, like the operating system, for example.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a134926_intel_secure_boot',
        100
  );

  // Add new attribute a133025_thermal_monitoring_tec
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133025_thermal_monitoring_tec', array(
    'label'                      => 'Thermal Monitoring Technologies',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a133025_thermal_monitoring_tec',
        100
  );

  // Add new attribute a133034_processor_package_size
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133034_processor_package_size', array(
    'label'                      => 'Processor package size',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a133034_processor_package_size',
        100
  );

  // Add new attribute a133040_processor_code
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133040_processor_code', array(
    'label'                      => 'Processor code',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a133040_processor_code',
        100
  );

  // Add new attribute a133081_uart
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133081_uart', array(
    'label'                      => 'UART',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a133081_uart',
        100
  );

  // Add new attribute a133052_cpu_configuration_max
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133052_cpu_configuration_max', array(
    'label'                      => 'CPU configuration (max)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a133052_cpu_configuration_max',
        100
  );

  // Add new attribute a130199_intel_hd_graphics
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a130199_intel_hd_graphics', array(
    'label'                      => 'Intel HD Graphics',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Intel® HD Graphics Power Great Visuals',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a130199_intel_hd_graphics',
        100
  );

  // Add new attribute a130273_intel_clear_video_hd_t
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a130273_intel_clear_video_hd_t', array(
    'label'                      => 'Intel® Clear Video HD Technology (Intel® CVT HD)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'See the World More Vividly',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a130273_intel_clear_video_hd_t',
        100
  );

  // Add new attribute a130274_intel_clear_video_tech
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a130274_intel_clear_video_tech', array(
    'label'                      => 'Intel Clear Video Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a130274_intel_clear_video_tech',
        100
  );

  // Add new attribute a56931_intel_vpro_technology
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a56931_intel_vpro_technology', array(
    'label'                      => 'Intel® vPro™ Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Leading-Edge Security for an Unwired Workplace',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a56931_intel_vpro_technology',
        100
  );

  // Add new attribute a56930_intel_identity_protecti
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a56930_intel_identity_protecti', array(
    'label'                      => 'Intel® Identity Protection Technology (Intel® IPT)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Protect Your Identity and Business',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a56930_intel_identity_protecti',
        100
  );

  // Add new attribute a56926_intel_smart_connect_tec
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a56926_intel_smart_connect_tec', array(
    'label'                      => 'Intel Smart Connect Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Intel Smart Connect Technology continually and automatically updates your e-mail, applications, and social networks, even when the system is asleep.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a56926_intel_smart_connect_tec',
        100
  );

  // Add new attribute a56927_intel_antitheft_technol
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a56927_intel_antitheft_technol', array(
    'label'                      => 'Intel® Anti-Theft Technology (Intel® AT)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Disable access to missing laptops',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a56927_intel_antitheft_technol',
        100
  );

  // Add new attribute a56929_intel_smart_response_te
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a56929_intel_smart_response_te', array(
    'label'                      => 'Intel® Smart Response Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Fast Access to Frequently Used Applications',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a56929_intel_smart_response_te',
        100
  );

  // Add new attribute a130275_intel_intru_3d_technol
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a130275_intel_intru_3d_technol', array(
    'label'                      => 'Intel® InTru™ 3D Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Add a new dimension to your viewing experience. Now you can play games and watch your favorite 3D movies with Blu-ray Stereo 3D playback using passive or active shutter 3D glasses. InTru™ 3D technology by Intel in association with Dreamworks delivers it all in 1080p full high-definition resolution on your TV over HDMI 1.4.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a130275_intel_intru_3d_technol',
        100
  );

  // Add new attribute a130276_intel_insider
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a130276_intel_insider', array(
    'label'                      => 'Intel® Insider™',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Experience a whole new level of premium HD entertainment with Intel® Insider™—available exclusively with 5th generation Intel® Core™ processors.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a130276_intel_insider',
        100
  );

  // Add new attribute a105683_enhanced_intel_speedst
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a105683_enhanced_intel_speedst', array(
    'label'                      => 'Enhanced Intel SpeedStep Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a105683_enhanced_intel_speedst',
        100
  );

  // Add new attribute a129306_intel_builtin_visuals_
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a129306_intel_builtin_visuals_', array(
    'label'                      => 'Intel® Built-in Visuals Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Realistic Graphics with Built-in Visuals',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a129306_intel_builtin_visuals_',
        100
  );

  // Add new attribute a133055_embedded_options_avail
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133055_embedded_options_avail', array(
    'label'                      => 'Embedded options available',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a133055_embedded_options_avail',
        100
  );

  // Add new attribute a90915_intel_high_definition_a
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a90915_intel_high_definition_a', array(
    'label'                      => 'Intel® High Definition Audio (Intel® HD Audio)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Integrated Audio for Today and Tomorrow',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a90915_intel_high_definition_a',
        100
  );

  // Add new attribute a79279_intel_small_business_ad
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a79279_intel_small_business_ad', array(
    'label'                      => 'Intel® Small Business Advantage (Intel® SBA)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Solutions to Help You Focus on Your Small Business',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a79279_intel_small_business_ad',
        100
  );

  // Add new attribute a130284_intel_quick_sync_video
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a130284_intel_quick_sync_video', array(
    'label'                      => 'Intel® Quick Sync Video Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Create, Edit, and Share Video in a Flash',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a130284_intel_quick_sync_video',
        100
  );

  // Add new attribute a56933_intel_hyper_threading_t
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a56933_intel_hyper_threading_t', array(
    'label'                      => 'Intel® Hyper Threading Technology (Intel® HT Technology)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Improved Performance for Threaded Software',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a56933_intel_hyper_threading_t',
        100
  );

  // Add new attribute a57383_intel_turbo_boost_techn
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57383_intel_turbo_boost_techn', array(
    'label'                      => 'Intel® Turbo Boost Technology',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Higher Performance When You Need It Most',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a57383_intel_turbo_boost_techn',
        100
  );

  // Add new attribute a132823_intel_smart_cache
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132823_intel_smart_cache', array(
    'label'                      => 'Intel® Smart Cache',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Improve Responsiveness with Faster Data Access',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132823_intel_smart_cache',
        100
  );

  // Add new attribute a1187_battery_technology
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a1187_battery_technology', array(
    'label'                      => 'Battery technology',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The type of battery in the device, e.g.  nickel–cadmium (NiCd).',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g7946_battery',
        'a1187_battery_technology',
        100
  );

  // Add new attribute a16197_number_of_battery_cells
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a16197_number_of_battery_cells', array(
    'label'                      => 'Number of battery cells',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g7946_battery',
        'a16197_number_of_battery_cells',
        100
  );

  // Add new attribute a11249_battery_life_max
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a11249_battery_life_max', array(
    'label'                      => 'Battery life (max)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Maximum battery life. Mobile Mark™ (2002) is the standard often used to benchmark battery life time.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g7946_battery',
        'a11249_battery_life_max',
        100
  );

  // Add new attribute a50479_battery_capacity_watt_h
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a50479_battery_capacity_watt_h', array(
    'label'                      => 'Battery capacity (Watt hours)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g7946_battery',
        'a50479_battery_capacity_watt_h',
        100
  );

  // Add new attribute a2652_ac_adapter_power
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2652_ac_adapter_power', array(
    'label'                      => 'AC adapter power',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g109_power',
        'a2652_ac_adapter_power',
        100
  );

  // Add new attribute a2649_ac_adapter_frequency
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2649_ac_adapter_frequency', array(
    'label'                      => 'AC adapter frequency',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g109_power',
        'a2649_ac_adapter_frequency',
        100
  );

  // Add new attribute a2648_ac_adapter_input_voltage
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2648_ac_adapter_input_voltage', array(
    'label'                      => 'AC adapter input voltage',
    'input'                      => 'price',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g109_power',
        'a2648_ac_adapter_input_voltage',
        100
  );

  // Add new attribute a93130_password_protection
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a93130_password_protection', array(
    'label'                      => 'Password protection',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The computer, smartphone etc. can only be used when the correct password is entered, therefore making it more secure.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2686_security',
        'a93130_password_protection',
        100
  );

  // Add new attribute a44805_password_protection_typ
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a44805_password_protection_typ', array(
    'label'                      => 'Password protection type',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2686_security',
        'a44805_password_protection_typ',
        100
  );

  // Add new attribute a2815_operating_temperature_tt
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2815_operating_temperature_tt', array(
    'label'                      => 'Operating temperature (T-T)',
    'input'                      => 'price',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The minimum and maximum temperatures at which the product can be safely operated.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1403_operational_conditions',
        'a2815_operating_temperature_tt',
        100
  );

  // Add new attribute a2024_storage_temperature_tt
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2024_storage_temperature_tt', array(
    'label'                      => 'Storage temperature (T-T)',
    'input'                      => 'price',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The minimum and maximum temperatures at which the product can be safely stored.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1403_operational_conditions',
        'a2024_storage_temperature_tt',
        100
  );

  // Add new attribute a2021_operating_relative_humid
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2021_operating_relative_humid', array(
    'label'                      => 'Operating relative humidity (H-H)',
    'input'                      => 'price',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1403_operational_conditions',
        'a2021_operating_relative_humid',
        100
  );

  // Add new attribute a12193_storage_relative_humidi
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12193_storage_relative_humidi', array(
    'label'                      => 'Storage relative humidity (H-H)',
    'input'                      => 'price',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1403_operational_conditions',
        'a12193_storage_relative_humidi',
        100
  );

  // Add new attribute a6826_operating_altitude
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a6826_operating_altitude', array(
    'label'                      => 'Operating altitude',
    'input'                      => 'price',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1403_operational_conditions',
        'a6826_operating_altitude',
        100
  );

  // Add new attribute a49148_rohs_compliance
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a49148_rohs_compliance', array(
    'label'                      => 'RoHS compliance',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The Restriction of Hazardous Substances (RoHS) Directive aims to restrict certain dangerous substances commonly used in electrical and electronic equipment. Any RoHS compliant product is tested for the presence of Lead (Pb), Cadmium (Cd), Mercury (Hg), Hexavalent chromium (Hex-Cr), Polybrominated biphenyls (PBB), and Polybrominated diphenyl ethers (PBDE). PBB and PBDE are flame retardants used in several plastics. For Cadmium and Hexavalent chromium, there must be less than 0.01% of the substance by weight at raw homogeneous materials level. For Lead, PBB, and PBDE, there must be no more than 0.1% of the material, when calculated by weight at raw homogeneous materials. Any RoHS compliant component must have 100 ppm or less of mercury and the mercury must not have been intentionally added to the component. In the EU, some military and medical equipment are exempt from RoHS compliance.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2687_certificates',
        'a49148_rohs_compliance',
        100
  );

  // Add new attribute a6240_width
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a6240_width', array(
    'label'                      => 'Width',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The measurement or extent of something from side to side.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g108_weight__dimensions',
        'a6240_width',
        100
  );

  // Add new attribute a6237_depth
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a6237_depth', array(
    'label'                      => 'Depth',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The distance from the front to the back of something.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g108_weight__dimensions',
        'a6237_depth',
        100
  );

  // Add new attribute a6239_height
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a6239_height', array(
    'label'                      => 'Height',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The measurement of the product from head to foot or from base to top.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g108_weight__dimensions',
        'a6239_height',
        100
  );

  // Add new attribute a4922_weight
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a4922_weight', array(
    'label'                      => 'Weight',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Weight of the product without packaging (net weight). If possible, the net weight is given including standard accessories and supplies. Please note that sometimes the manufacturer leaves out the weight of accessories and/or supplies.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g108_weight__dimensions',
        'a4922_weight',
        100
  );

  // Add new attribute a57720_manual
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57720_manual', array(
    'label'                      => 'Manual',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26489_packaging_content',
        'a57720_manual',
        100
  );

  // Add new attribute a92457_power_cord_included
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a92457_power_cord_included', array(
    'label'                      => 'Power cord included',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26489_packaging_content',
        'a92457_power_cord_included',
        100
  );

  // Add new attribute a57811_ac_adapter_included
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57811_ac_adapter_included', array(
    'label'                      => 'AC adapter included',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26489_packaging_content',
        'a57811_ac_adapter_included',
        100
  );

  // Add new attribute a133013_fsb_parity
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133013_fsb_parity', array(
    'label'                      => 'FSB Parity',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a133013_fsb_parity',
        100
  );

  // Add new attribute a134988_configurable_tdpdown_f
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a134988_configurable_tdpdown_f', array(
    'label'                      => 'Configurable TDP-down frequency',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a134988_configurable_tdpdown_f',
        100
  );

  // Add new attribute a132809_bus_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132809_bus_type', array(
    'label'                      => 'Bus type',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a132809_bus_type',
        100
  );

  // Add new attribute a73736_memory_layout_slots_x_s
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a73736_memory_layout_slots_x_s', array(
    'label'                      => 'Memory layout (slots x size)',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The slots and size of the memory for the CPU.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g106_memory',
        'a73736_memory_layout_slots_x_s',
        100
  );

  // Add new attribute a33265_hard_drive_capacity
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a33265_hard_drive_capacity', array(
    'label'                      => 'Hard drive capacity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The maximum storage capacity of the hard disk, usually measured in bytes e.g. GB.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a33265_hard_drive_capacity',
        100
  );

  // Add new attribute a132913_onboard_graphics_adapt
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132913_onboard_graphics_adapt', array(
    'label'                      => 'On-board graphics adapter dynamic frequency (max)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a132913_onboard_graphics_adapt',
        100
  );

  // Add new attribute a132951_onboard_graphics_adapt
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132951_onboard_graphics_adapt', array(
    'label'                      => 'On-board graphics adapter DirectX version',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a132951_onboard_graphics_adapt',
        100
  );

  // Add new attribute a1195_audio_system
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a1195_audio_system', array(
    'label'                      => 'Audio system',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'System used to play music or speech.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g110_audio',
        'a1195_audio_system',
        100
  );

  // Add new attribute a20451_bluetooth_version
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a20451_bluetooth_version', array(
    'label'                      => 'Bluetooth version',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The type of bluetooth technology in the product e.g. Bluetooth Smart (v4.0).',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a20451_bluetooth_version',
        100
  );

  // Add new attribute a12830_s/pdif_out_port
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12830_s/pdif_out_port', array(
    'label'                      => 'S/PDIF out port',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'S/PDIF (Sony/Philips Digital Interconnect Format) is a digital audio interconnect used in consumer audio equipment over relatively short distances. The signal is transmitted over either a coaxial cable with RCA connectors or a fibre optic cable with TOSLINK connectors.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a12830_s/pdif_out_port',
        100
  );

  // Add new attribute a20422_expresscard_slot
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a20422_expresscard_slot', array(
    'label'                      => 'ExpressCard slot',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Port for an ExpressCard, which contain electronic circuitry and connectors to which external devices (peripherals) can be connected. The ExpressCard standard has replaced the PC Card (also known as PCMCIA) standard and has been used since 2006.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a20422_expresscard_slot',
        100
  );

  // Add new attribute a12839_cardbus_pcmcia_slots_qu
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12839_cardbus_pcmcia_slots_qu', array(
    'label'                      => 'CardBus PCMCIA slots quantity',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The number of slots for CardBus PCMCIA.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a12839_cardbus_pcmcia_slots_qu',
        100
  );

  // Add new attribute a12846_cardbus_pcmcia_slot_typ
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12846_cardbus_pcmcia_slot_typ', array(
    'label'                      => 'CardBus PCMCIA slot type',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The type of slots for CardBus PCMCIA.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a12846_cardbus_pcmcia_slot_typ',
        100
  );

  // Add new attribute a12840_smartcard_slot
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12840_smartcard_slot', array(
    'label'                      => 'SmartCard slot',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'A smart card, chip card, or integrated circuit card (ICC) is any pocket-sized card with embedded integrated circuits. Smart cards can provide identification, authentication, data storage and application processing. Smart card slots can be found in various electronic devices e.g. thin clients.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a12840_smartcard_slot',
        100
  );

  // Add new attribute a46052_fullsize_keyboard
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a46052_fullsize_keyboard', array(
    'label'                      => 'Full-size keyboard',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g762_keyboard',
        'a46052_fullsize_keyboard',
        100
  );

  // Add new attribute a72363_islandstyle_keyboard
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a72363_islandstyle_keyboard', array(
    'label'                      => 'Island-style keyboard',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g762_keyboard',
        'a72363_islandstyle_keyboard',
        100
  );

  // Add new attribute a133057_graphics__imc_lithogra
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133057_graphics__imc_lithogra', array(
    'label'                      => 'Graphics & IMC lithography',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a133057_graphics__imc_lithogra',
        100
  );

  // Add new attribute a133037_supported_instruction_
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133037_supported_instruction_', array(
    'label'                      => 'Supported instruction sets',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a133037_supported_instruction_',
        100
  );

  // Add new attribute a135071_intel_identity_protect
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135071_intel_identity_protect', array(
    'label'                      => 'Intel Identity Protection Technology version',
    'input'                      => 'text',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a135071_intel_identity_protect',
        100
  );

  // Add new attribute a135074_intel_smart_response_t
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135074_intel_smart_response_t', array(
    'label'                      => 'Intel Smart Response Technology version',
    'input'                      => 'text',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a135074_intel_smart_response_t',
        100
  );

  // Add new attribute a135165_intel_fdi_technology
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135165_intel_fdi_technology', array(
    'label'                      => 'Intel FDI Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a135165_intel_fdi_technology',
        100
  );

  // Add new attribute a140196_intel_fast_memory_acce
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a140196_intel_fast_memory_acce', array(
    'label'                      => 'Intel Fast Memory Access',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a140196_intel_fast_memory_acce',
        100
  );

  // Add new attribute a135150_intel_dual_display_cap
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135150_intel_dual_display_cap', array(
    'label'                      => 'Intel Dual Display Capable Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a135150_intel_dual_display_cap',
        100
  );

  // Add new attribute a135077_intel_secure_key_techn
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135077_intel_secure_key_techn', array(
    'label'                      => 'Intel Secure Key Technology version',
    'input'                      => 'text',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a135077_intel_secure_key_techn',
        100
  );

  // Add new attribute a135078_intel_small_business_a
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135078_intel_small_business_a', array(
    'label'                      => 'Intel Small Business Advantage (SBA) version',
    'input'                      => 'text',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a135078_intel_small_business_a',
        100
  );

  // Add new attribute a135124_intel_tsxni_version
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135124_intel_tsxni_version', array(
    'label'                      => 'Intel TSX-NI version',
    'input'                      => 'text',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a135124_intel_tsxni_version',
        100
  );

  // Add new attribute a56925_intel_my_wifi_technolog
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a56925_intel_my_wifi_technolog', array(
    'label'                      => 'Intel® My WiFi Technology (Intel® MWT)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Intel® My WiFi Technology (Intel® MWT) uses an extensible software architecture that allows the operating system to think the notebook has two Wi-Fi radios performing similar, but different, functions. Each virtual Wi-Fi radio is assigned a TCP/IP stack.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a56925_intel_my_wifi_technolog',
        100
  );

  // Add new attribute a132838_intel_demand_based_swi
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132838_intel_demand_based_swi', array(
    'label'                      => 'Intel Demand Based Switching',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132838_intel_demand_based_swi',
        100
  );

  // Add new attribute a132821_intel_flex_memory_acce
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132821_intel_flex_memory_acce', array(
    'label'                      => 'Intel Flex Memory Access',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132821_intel_flex_memory_acce',
        100
  );

  // Add new attribute a16306_cable_lock_slot
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a16306_cable_lock_slot', array(
    'label'                      => 'Cable lock slot',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Holes on the edge of devices through which a cable lock can be passed, so the device can be locked to a desk etc.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2686_security',
        'a16306_cable_lock_slot',
        100
  );

  // Add new attribute a49798_epeat_compliance
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a49798_epeat_compliance', array(
    'label'                      => 'EPEAT compliance',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'EPEAT (Electronic Product Environmental Assessment Tool) is a way to measure the impact of a product on the environment.  The most environmentally-friendly products are given a Gold rating, and the least given a Bronze.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2687_certificates',
        'a49798_epeat_compliance',
        100
  );

  // Add new attribute a39564_energy_star_certified
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a39564_energy_star_certified', array(
    'label'                      => 'Energy Star certified',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'This indicates that the product is compliant with Energy Star, which is an international standard for energy-efficient consumer products.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2687_certificates',
        'a39564_energy_star_certified',
        100
  );

  // Add new attribute a59741_number_of_ssds_installe
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a59741_number_of_ssds_installe', array(
    'label'                      => 'Number of SSDs installed',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a59741_number_of_ssds_installe',
        100
  );

  // Add new attribute a26443_solidstate_drive_capaci
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a26443_solidstate_drive_capaci', array(
    'label'                      => 'Solid-state drive capacity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The Solid State Drive\'s data storage capacity.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a26443_solidstate_drive_capaci',
        100
  );

  // Add new attribute a26442_solidstate_drive_interf
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a26442_solidstate_drive_interf', array(
    'label'                      => 'Solid-state drive interface',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The type of interface(s) with which the Solid State Drive is connected to other hardware.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a26442_solidstate_drive_interf',
        100
  );

  // Add new attribute a148373_ssd_form_factor
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a148373_ssd_form_factor', array(
    'label'                      => 'SSD form factor',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a148373_ssd_form_factor',
        100
  );

  // Add new attribute a132954_onboard_graphics_adapt
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132954_onboard_graphics_adapt', array(
    'label'                      => 'On-board graphics adapter OpenGL version',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a132954_onboard_graphics_adapt',
        100
  );

  // Add new attribute a135141_onboard_graphics_adapt
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135141_onboard_graphics_adapt', array(
    'label'                      => 'On-board graphics adapter ID',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a135141_onboard_graphics_adapt',
        100
  );

  // Add new attribute a138254_usb_3.1_3.1_gen_2_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a138254_usb_3.1_3.1_gen_2_type', array(
    'label'                      => 'USB 3.1 (3.1 Gen 2) Type-C ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Number of USB 3.1 (3.1 Gen 2) ports with Type-C connector. USB 3.1 (3.1 Gen 2) is USB interface version which supports transfer rate up to 10 Gbit/s (SuperSpeed+). USB Type-C connector has symmetrical design architecture and compatible with Thunderbolt 3 interface.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a138254_usb_3.1_3.1_gen_2_type',
        100
  );

  // Add new attribute a71309_keyboard_backlit
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a71309_keyboard_backlit', array(
    'label'                      => 'Keyboard backlit',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g762_keyboard',
        'a71309_keyboard_backlit',
        100
  );

  // Add new attribute a135076_intel_stable_image_pla
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135076_intel_stable_image_pla', array(
    'label'                      => 'Intel Stable Image Platform Program (SIPP) version',
    'input'                      => 'text',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a135076_intel_stable_image_pla',
        100
  );

  // Add new attribute a132850_intel_software_guard_e
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132850_intel_software_guard_e', array(
    'label'                      => 'Intel Software Guard Extensions (Intel SGX)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132850_intel_software_guard_e',
        100
  );

  // Add new attribute a237656_hp_mobile_connect
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a237656_hp_mobile_connect', array(
    'label'                      => 'HP Mobile Connect',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Go beyond Wi-Fi with mobile broadband, ready right out-of-the-box.  With the included data package, just power on, register, and connect. No contract required.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a237656_hp_mobile_connect',
        100
  );

  // Add new attribute a238836_hp_eprint
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a238836_hp_eprint', array(
    'label'                      => 'HP ePrint',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a238836_hp_eprint',
        100
  );

  // Add new attribute a238875_hp_recovery_manager
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a238875_hp_recovery_manager', array(
    'label'                      => 'HP Recovery Manager',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Use HP Recovery Manager to reinstall some of the hardware drivers and software programs that originally came with your HP computer. Not all software is available to be reinstalled.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a238875_hp_recovery_manager',
        100
  );

  // Add new attribute a237845_hp_support_assistant
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a237845_hp_support_assistant', array(
    'label'                      => 'HP Support Assistant',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Get answers to everyday support questions about your HP PC and printers with this free self-help tool built into your computer. Optimized to provide an enhanced support experience, it’s automatically updated and ready to go the first time you boot up. And when you need hands-on help, you can use the tool to directly connect to HP Support.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a237845_hp_support_assistant',
        100
  );

  // Add new attribute a238835_hp_touchpoint_manager
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a238835_hp_touchpoint_manager', array(
    'label'                      => 'HP Touchpoint Manager',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a238835_hp_touchpoint_manager',
        100
  );

  // Add new attribute a237851_hp_software_provided
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a237851_hp_software_provided', array(
    'label'                      => 'HP Software provided',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a237851_hp_software_provided',
        100
  );

  // Add new attribute a244522_hp_segment
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a244522_hp_segment', array(
    'label'                      => 'HP segment',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a244522_hp_segment',
        100
  );

  // Add new attribute a20477_trusted_platform_module
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a20477_trusted_platform_module', array(
    'label'                      => 'Trusted Platform Module (TPM)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2686_security',
        'a20477_trusted_platform_module',
        100
  );

  // Add new attribute a157554_intel_segment_tagging
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a157554_intel_segment_tagging', array(
    'label'                      => 'Intel segment tagging',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10674_design',
        'a157554_intel_segment_tagging',
        100
  );

  // Add new attribute a99902_display_surface
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a99902_display_surface', array(
    'label'                      => 'Display surface',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a99902_display_surface',
        100
  );

  // Add new attribute a7237_speakers_manufacturer
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a7237_speakers_manufacturer', array(
    'label'                      => 'Speakers manufacturer',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g110_audio',
        'a7237_speakers_manufacturer',
        100
  );

  // Add new attribute a95636_4g_standard
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a95636_4g_standard', array(
    'label'                      => '4G standard',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a95636_4g_standard',
        100
  );

  // Add new attribute a79336_near_field_communicatio
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a79336_near_field_communicatio', array(
    'label'                      => 'Near Field Communication (NFC)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a79336_near_field_communicatio',
        100
  );

  // Add new attribute a95635_3g_standards
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a95635_3g_standards', array(
    'label'                      => '3G standards',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a95635_3g_standards',
        100
  );

  // Add new attribute a31107_optional_operating_syst
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a31107_optional_operating_syst', array(
    'label'                      => 'Optional operating system supplied',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2933_software',
        'a31107_optional_operating_syst',
        100
  );

  // Add new attribute a132843_intel_tsxni
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132843_intel_tsxni', array(
    'label'                      => 'Intel TSX-NI',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132843_intel_tsxni',
        100
  );

  // Add new attribute a133071_configurable_tdpdown
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a133071_configurable_tdpdown', array(
    'label'                      => 'Configurable TDP-down',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g35_processor',
        'a133071_configurable_tdpdown',
        100
  );

  // Add new attribute a2549_memory_slots
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2549_memory_slots', array(
    'label'                      => 'Memory slots',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Number and type of memory expansion slots, including connector and memory module descriptions.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g106_memory',
        'a2549_memory_slots',
        100
  );

  // Add new attribute a109421_discrete_graphics_adap
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a109421_discrete_graphics_adap', array(
    'label'                      => 'Discrete graphics adapter memory',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a109421_discrete_graphics_adap',
        100
  );

  // Add new attribute a123174_nvidia_gsync
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a123174_nvidia_gsync', array(
    'label'                      => 'NVIDIA G-SYNC',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a123174_nvidia_gsync',
        100
  );

  // Add new attribute a123175_nvidia_gameworks_vr
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a123175_nvidia_gameworks_vr', array(
    'label'                      => 'NVIDIA GameWorks VR',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a123175_nvidia_gameworks_vr',
        100
  );

  // Add new attribute a2655_battery_capacity
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2655_battery_capacity', array(
    'label'                      => 'Battery capacity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g7946_battery',
        'a2655_battery_capacity',
        100
  );

  // Add new attribute a27273_height_front
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a27273_height_front', array(
    'label'                      => 'Height (front)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g108_weight__dimensions',
        'a27273_height_front',
        100
  );

  // Add new attribute a27274_height_rear
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a27274_height_rear', array(
    'label'                      => 'Height (rear)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g108_weight__dimensions',
        'a27274_height_rear',
        100
  );

  // Add new attribute a59777_market_positioning
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a59777_market_positioning', array(
    'label'                      => 'Market positioning',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10674_design',
        'a59777_market_positioning',
        100
  );

  // Add new attribute a53951_discrete_graphics_memor
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a53951_discrete_graphics_memor', array(
    'label'                      => 'Discrete graphics memory type',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a53951_discrete_graphics_memor',
        100
  );

  // Add new attribute a94689_number_of_discrete_grap
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a94689_number_of_discrete_grap', array(
    'label'                      => 'Number of discrete graphics adapters installed',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a94689_number_of_discrete_grap',
        100
  );

  // Add new attribute a138252_usb_3.1_3.1_gen_2_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a138252_usb_3.1_3.1_gen_2_type', array(
    'label'                      => 'USB 3.1 (3.1 Gen 2) Type-A ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a138252_usb_3.1_3.1_gen_2_type',
        100
  );

  // Add new attribute a93106_cable_lock_slot_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a93106_cable_lock_slot_type', array(
    'label'                      => 'Cable lock slot type',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2686_security',
        'a93106_cable_lock_slot_type',
        100
  );

  // Add new attribute a48349_hdmi_version
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a48349_hdmi_version', array(
    'label'                      => 'HDMI version',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a48349_hdmi_version',
        100
  );

  // Add new attribute a49942_spill_resistant_keyboar
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a49942_spill_resistant_keyboar', array(
    'label'                      => 'Spill resistant keyboard',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g762_keyboard',
        'a49942_spill_resistant_keyboar',
        100
  );

  // Add new attribute a71486_recovery_operating_syst
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a71486_recovery_operating_syst', array(
    'label'                      => 'Recovery operating system',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2933_software',
        'a71486_recovery_operating_syst',
        100
  );

  // Add new attribute a174076_front_camera_signal_fo
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a174076_front_camera_signal_fo', array(
    'label'                      => 'Front camera signal format',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g3024_camera',
        'a174076_front_camera_signal_fo',
        100
  );

  // Add new attribute a138251_usb_3.0_3.1_gen_1_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a138251_usb_3.0_3.1_gen_1_type', array(
    'label'                      => 'USB 3.0 (3.1 Gen 1) Type-C ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Number of USB 3.0 (3.1 Gen 1) ports with Type-C connector. USB 3.0 (3.1 Gen 1) is USB interface version which supports transfer rate up to 5 Gbit/s (SuperSpeed). USB Type-C connector has symmetrical design architecture and compatible with Thunderbolt 3 interface.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a138251_usb_3.0_3.1_gen_1_type',
        100
  );

  // Add new attribute a237650_hp_3d_driveguard
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a237650_hp_3d_driveguard', array(
    'label'                      => 'HP 3D DriveGuard',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a237650_hp_3d_driveguard',
        100
  );

  // Add new attribute a237654_hp_audio_boost
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a237654_hp_audio_boost', array(
    'label'                      => 'HP Audio Boost',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Take your audio experience to the next level with smart amplification technology giving you loud, clear and dynamic sound.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a237654_hp_audio_boost',
        100
  );

  // Add new attribute a237651_hp_security_tools
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a237651_hp_security_tools', array(
    'label'                      => 'HP Security tools',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a237651_hp_security_tools',
        100
  );

  // Add new attribute a237849_hp_management_tools
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a237849_hp_management_tools', array(
    'label'                      => 'HP Management tools',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a237849_hp_management_tools',
        100
  );

  // Add new attribute a2650_ac_adapter_output_voltag
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2650_ac_adapter_output_voltag', array(
    'label'                      => 'AC adapter output voltage',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g109_power',
        'a2650_ac_adapter_output_voltag',
        100
  );

  // Add new attribute a12823_firewire_ieee_1394_port
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a12823_firewire_ieee_1394_port', array(
    'label'                      => 'Firewire (IEEE 1394) ports',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Number of IEEE 1394/FireWire ports (connecting interfaces) in the device. The IEEE 1394 interface is a serial bus interface standard for high-speed communications and isochronous real-time data transfer. It was developed in the late 1980s and early 1990s by Apple, who called it FireWire. The 1394 interface is comparable to USB though USB has more market share. Apple first included FireWire in some of its 1999 models, and most Apple computers since the year 2000 have included FireWire ports, though, as of 2013, only the 800 version (IEEE-1394b).',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a12823_firewire_ieee_1394_port',
        100
  );

  // Add new attribute a1191_pixel_pitch
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a1191_pixel_pitch', array(
    'label'                      => 'Pixel pitch',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a1191_pixel_pitch',
        100
  );

  // Add new attribute a45456_maximum_internal_memory
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a45456_maximum_internal_memory', array(
    'label'                      => 'Maximum internal memory (64-bit)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g106_memory',
        'a45456_maximum_internal_memory',
        100
  );

  // Add new attribute a157541_audio_chip
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a157541_audio_chip', array(
    'label'                      => 'Audio chip',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g110_audio',
        'a157541_audio_chip',
        100
  );

  // Add new attribute a91104_front_camera_resolution
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a91104_front_camera_resolution', array(
    'label'                      => 'Front camera resolution',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g3024_camera',
        'a91104_front_camera_resolution',
        100
  );

  // Add new attribute a86959_usb_sleepandcharge_port
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a86959_usb_sleepandcharge_port', array(
    'label'                      => 'USB Sleep-and-Charge ports',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a86959_usb_sleepandcharge_port',
        100
  );

  // Add new attribute a86958_usb_sleepandcharge
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a86958_usb_sleepandcharge', array(
    'label'                      => 'USB Sleep-and-Charge',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a86958_usb_sleepandcharge',
        100
  );

  // Add new attribute a31108_drivers_included
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a31108_drivers_included', array(
    'label'                      => 'Drivers included',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2933_software',
        'a31108_drivers_included',
        100
  );

  // Add new attribute a57279_battery_voltage
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57279_battery_voltage', array(
    'label'                      => 'Battery voltage',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g7946_battery',
        'a57279_battery_voltage',
        100
  );

  // Add new attribute a2651_ac_adapter_output_curren
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2651_ac_adapter_output_curren', array(
    'label'                      => 'AC adapter output current',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g109_power',
        'a2651_ac_adapter_output_curren',
        100
  );

  // Add new attribute a4638_nonoperating_altitude
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a4638_nonoperating_altitude', array(
    'label'                      => 'Non-operating altitude',
    'input'                      => 'price',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1403_operational_conditions',
        'a4638_nonoperating_altitude',
        100
  );

  // Add new attribute a21025_fingerprint_reader
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a21025_fingerprint_reader', array(
    'label'                      => 'Fingerprint reader',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'A scanner used to identify a person\'s fingerprint for security purposes. After a sample is taken, access to a computer or other system is granted if the fingerprint matches the stored sample. A PIN may also be used with the fingerprint sample.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2686_security',
        'a21025_fingerprint_reader',
        100
  );

  // Add new attribute a57721_touch_technology
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57721_touch_technology', array(
    'label'                      => 'Touch technology',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Technology of touch sensetive screen e.g. dual-touch.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a57721_touch_technology',
        100
  );

  // Add new attribute a71509_touchscreen_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a71509_touchscreen_type', array(
    'label'                      => 'Touchscreen type',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'There are a variety of touchscreen technologies that have different methods of sensing touch, including resistive and capacitive.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a71509_touchscreen_type',
        100
  );

  // Add new attribute a91100_rear_camera
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a91100_rear_camera', array(
    'label'                      => 'Rear camera',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Camera at the back of the product.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g3024_camera',
        'a91100_rear_camera',
        100
  );

  // Add new attribute a91101_rear_camera_resolution_
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a91101_rear_camera_resolution_', array(
    'label'                      => 'Rear camera resolution (numeric)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g3024_camera',
        'a91101_rear_camera_resolution_',
        100
  );

  // Add new attribute a57409_gps_satellite
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57409_gps_satellite', array(
    'label'                      => 'GPS (satellite)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Global Positioning System (GPS) is a space-based satellite navigation system that provides location and time information in all weather conditions, anywhere on or near the Earth where there is an unobstructed line of sight to four or more GPS satellites. The system provides critical capabilities to military, civil and commercial users around the world. It is maintained by the United States government and is freely accessible to anyone with a GPS receiver.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10675_performance',
        'a57409_gps_satellite',
        100
  );

  // Add new attribute a237663_hp_stylus_pen_type
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a237663_hp_stylus_pen_type', array(
    'label'                      => 'HP stylus pen type',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a237663_hp_stylus_pen_type',
        100
  );

  // Add new attribute a57810_quick_start_guide
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57810_quick_start_guide', array(
    'label'                      => 'Quick start guide',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Instructions, either online, as a computer program or in paper form, to allow you to set up and start using the product quickly.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26489_packaging_content',
        'a57810_quick_start_guide',
        100
  );

  // Add new attribute a2683_graphics_adapter_family
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a2683_graphics_adapter_family', array(
    'label'                      => 'Graphics adapter family',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g6417_other_features',
        'a2683_graphics_adapter_family',
        100
  );

  // Add new attribute a105664_intel_virtualization_t
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a105664_intel_virtualization_t', array(
    'label'                      => 'Intel® Virtualization Technology (Intel® VT)',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Simplify Virtualization and Reduce Overheads',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g6417_other_features',
        'a105664_intel_virtualization_t',
        100
  );

  // Add new attribute a59776_warranty_card
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a59776_warranty_card', array(
    'label'                      => 'Warranty card',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26489_packaging_content',
        'a59776_warranty_card',
        100
  );

  // Add new attribute a154115_display_number_of_colo
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a154115_display_number_of_colo', array(
    'label'                      => 'Display number of colours',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g33_display',
        'a154115_display_number_of_colo',
        100
  );

  // Add new attribute a57710_flash_memory
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57710_flash_memory', array(
    'label'                      => 'Flash memory',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a57710_flash_memory',
        100
  );

  // Add new attribute a162945_audio_decoders
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a162945_audio_decoders', array(
    'label'                      => 'Audio decoders',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Device or software capable of decoding a digital stream of audio e.g. internet radio.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g110_audio',
        'a162945_audio_decoders',
        100
  );

  // Add new attribute a56916_power_autosensing
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a56916_power_autosensing', array(
    'label'                      => 'Power auto-sensing',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g109_power',
        'a56916_power_autosensing',
        100
  );

  // Add new attribute a237647_hp_jumpstart
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a237647_hp_jumpstart', array(
    'label'                      => 'HP JumpStart',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'When you boot up your PC for the first time, get a personal walkthrough during the setup and get an informative overview of your PC’s features.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a237647_hp_jumpstart',
        100
  );

  // Add new attribute a237788_hp_front_camera
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a237788_hp_front_camera', array(
    'label'                      => 'HP front camera',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a237788_hp_front_camera',
        100
  );

  // Add new attribute a69770_accelerometer
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a69770_accelerometer', array(
    'label'                      => 'Accelerometer',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10675_performance',
        'a69770_accelerometer',
        100
  );

  // Add new attribute a43560_package_width
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a43560_package_width', array(
    'label'                      => 'Package width',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The distance from one side of the packaging to the other.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g6416_packaging_data',
        'a43560_package_width',
        100
  );

  // Add new attribute a43555_package_depth
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a43555_package_depth', array(
    'label'                      => 'Package depth',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The distance from the front to the back of the packaging.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g6416_packaging_data',
        'a43555_package_depth',
        100
  );

  // Add new attribute a43556_package_height
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a43556_package_height', array(
    'label'                      => 'Package height',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The distance from the top to the bottom of the packaging.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g6416_packaging_data',
        'a43556_package_height',
        100
  );

  // Add new attribute a5037_package_weight
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a5037_package_weight', array(
    'label'                      => 'Package weight',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Weight of the packaged product.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g6416_packaging_data',
        'a5037_package_weight',
        100
  );

  // Add new attribute a135070_intel_smart_connect_te
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135070_intel_smart_connect_te', array(
    'label'                      => 'Intel Smart Connect Technology version',
    'input'                      => 'text',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a135070_intel_smart_connect_te',
        100
  );

  // Add new attribute a135114_intel_me_firmware_vers
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a135114_intel_me_firmware_vers', array(
    'label'                      => 'Intel ME Firmware Version',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a135114_intel_me_firmware_vers',
        100
  );

  // Add new attribute a90912_intel_matrix_storage_te
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a90912_intel_matrix_storage_te', array(
    'label'                      => 'Intel® Matrix Storage Technology (Intel® MST)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'New Levels of Hard Disk Drive Performance and Reliability',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a90912_intel_matrix_storage_te',
        100
  );

  // Add new attribute a93882_weight_tablet_mode
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a93882_weight_tablet_mode', array(
    'label'                      => 'Weight (tablet mode)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g108_weight__dimensions',
        'a93882_weight_tablet_mode',
        100
  );

  // Add new attribute a93881_width_tablet_mode
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a93881_width_tablet_mode', array(
    'label'                      => 'Width (tablet mode)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g108_weight__dimensions',
        'a93881_width_tablet_mode',
        100
  );

  // Add new attribute a93880_depth_tablet_mode
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a93880_depth_tablet_mode', array(
    'label'                      => 'Depth (tablet mode)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g108_weight__dimensions',
        'a93880_depth_tablet_mode',
        100
  );

  // Add new attribute a93879_height_tablet_mode
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a93879_height_tablet_mode', array(
    'label'                      => 'Height (tablet mode)',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g108_weight__dimensions',
        'a93879_height_tablet_mode',
        100
  );

  // Add new attribute a132826_intel_quickpath_interc
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a132826_intel_quickpath_interc', array(
    'label'                      => 'Intel QuickPath Interconnect',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a132826_intel_quickpath_interc',
        100
  );

  // Add new attribute a56928_intel_rapid_start_techn
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a56928_intel_rapid_start_techn', array(
    'label'                      => 'Intel® Rapid Start Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Get Up and Running in Seconds',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a56928_intel_rapid_start_techn',
        100
  );

  // Add new attribute a90918_intel_active_management
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a90918_intel_active_management', array(
    'label'                      => 'Intel® Active Management Technology (Intel® AMT)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Remotely and Automatically Increase Efficiency and Effectiveness',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a90918_intel_active_management',
        100
  );

  // Add new attribute a115111_flash_drive_interface
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a115111_flash_drive_interface', array(
    'label'                      => 'Flash drive interface',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a115111_flash_drive_interface',
        100
  );

  // Add new attribute a55421_micro_hdmi_ports_quanti
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a55421_micro_hdmi_ports_quanti', array(
    'label'                      => 'Micro HDMI ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a55421_micro_hdmi_ports_quanti',
        100
  );

  // Add new attribute a237789_hp_rear_camera
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a237789_hp_rear_camera', array(
    'label'                      => 'HP rear camera',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26477_brandspecific_features',
        'a237789_hp_rear_camera',
        100
  );

  // Add new attribute a57818_video_capturing_speed
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57818_video_capturing_speed', array(
    'label'                      => 'Video capturing speed',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g3024_camera',
        'a57818_video_capturing_speed',
        100
  );

  // Add new attribute a101350_operating_system_langu
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a101350_operating_system_langu', array(
    'label'                      => 'Operating system language',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2933_software',
        'a101350_operating_system_langu',
        100
  );

  // Add new attribute a31109_trial_software
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a31109_trial_software', array(
    'label'                      => 'Trial software',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2933_software',
        'a31109_trial_software',
        100
  );

  // Add new attribute a41669_mini_displayports_quant
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a41669_mini_displayports_quant', array(
    'label'                      => 'Mini DisplayPorts quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a41669_mini_displayports_quant',
        100
  );

  // Add new attribute a210940_usb_typec_displayport_
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a210940_usb_typec_displayport_', array(
    'label'                      => 'USB Type-C DisplayPort Alternate Mode',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a210940_usb_typec_displayport_',
        100
  );

  // Add new attribute a16305_builtin_subwoofer
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a16305_builtin_subwoofer', array(
    'label'                      => 'Built-in subwoofer',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The product has built-in subwoofer, which is a loudspeaker component designed to reproduce very low bass frequencies.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g110_audio',
        'a16305_builtin_subwoofer',
        100
  );

  // Add new attribute a74321_hybrid_hard_drive_hhdd_
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a74321_hybrid_hard_drive_hhdd_', array(
    'label'                      => 'Hybrid hard drive (H-HDD) cache type',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g4124_storage',
        'a74321_hybrid_hard_drive_hhdd_',
        100
  );

  // Add new attribute a59743_disc_types_supported
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a59743_disc_types_supported', array(
    'label'                      => 'Disc types supported',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The types of disc which can be used with this device.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g32_optical_drive',
        'a59743_disc_types_supported',
        100
  );

  // Add new attribute a44480_wwan
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a44480_wwan', array(
    'label'                      => 'WWAN',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a44480_wwan',
        100
  );

  // Add new attribute a57717_microusb_2.0_ports_quan
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57717_microusb_2.0_ports_quan', array(
    'label'                      => 'Micro-USB 2.0 ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a57717_microusb_2.0_ports_quan',
        100
  );

  // Add new attribute a110846_microusb_3.0_ports_qua
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a110846_microusb_3.0_ports_qua', array(
    'label'                      => 'Micro-USB 3.0 ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a110846_microusb_3.0_ports_qua',
        100
  );

  // Add new attribute a26457_esata_ports_quantity
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a26457_esata_ports_quantity', array(
    'label'                      => 'eSATA ports quantity',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The number of ports (interfaces) for eSATA. eSATA is a  a computer bus interface that connects host bus adapters to mass storage devices such as hard disk drives and optical drives.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a26457_esata_ports_quantity',
        100
  );

  // Add new attribute a92145_intel_active_management
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a92145_intel_active_management', array(
    'label'                      => 'Intel Active Management Technology version',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a92145_intel_active_management',
        100
  );

  // Add new attribute a109424_intel_realsense_techno
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a109424_intel_realsense_techno', array(
    'label'                      => 'Intel® RealSense Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Digitally Interact with the Real World',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a109424_intel_realsense_techno',
        100
  );

  // Add new attribute a109425_intel_realsense_snapsh
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a109425_intel_realsense_snapsh', array(
    'label'                      => 'Intel RealSense Snapshot',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a109425_intel_realsense_snapsh',
        100
  );

  // Add new attribute a109426_intel_realsense_3d_cam
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a109426_intel_realsense_3d_cam', array(
    'label'                      => 'Intel RealSense 3D Camera',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a109426_intel_realsense_3d_cam',
        100
  );

  // Add new attribute a129317_thunderbolt_3_technolo
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a129317_thunderbolt_3_technolo', array(
    'label'                      => 'Thunderbolt™ 3 Technology',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Benefits of Thunderbolt™ Technology',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10385_processor_special_featu',
        'a129317_thunderbolt_3_technolo',
        100
  );

  // Add new attribute a49870_operating_shock
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a49870_operating_shock', array(
    'label'                      => 'Operating shock',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Environmental requirements for optimal protection from impact in operating mode',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1403_operational_conditions',
        'a49870_operating_shock',
        100
  );

  // Add new attribute a49871_nonoperating_shock
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a49871_nonoperating_shock', array(
    'label'                      => 'Non-operating shock',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Environmental requirements for optimal protection from impact in non-operating mode',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1403_operational_conditions',
        'a49871_nonoperating_shock',
        100
  );

  // Add new attribute a49872_operating_vibration
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a49872_operating_vibration', array(
    'label'                      => 'Operating vibration',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Environmental requirements for vibration in operating mode',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1403_operational_conditions',
        'a49872_operating_vibration',
        100
  );

  // Add new attribute a49873_nonoperating_vibration
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a49873_nonoperating_vibration', array(
    'label'                      => 'Non-operating vibration',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Environmental requirements for vibration in non-operating mode',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1403_operational_conditions',
        'a49873_nonoperating_vibration',
        100
  );

  // Add new attribute a45156_wifi_certified
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a45156_wifi_certified', array(
    'label'                      => 'Wi-Fi certified',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Wi-Fi is a popular technology to exchange data wirelessly, using radio waves, over a computer network.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2687_certificates',
        'a45156_wifi_certified',
        100
  );

  // Add new attribute a57719_cables_included
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57719_cables_included', array(
    'label'                      => 'Cables included',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26489_packaging_content',
        'a57719_cables_included',
        100
  );

  // Add new attribute a104174_gyroscope
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a104174_gyroscope', array(
    'label'                      => 'Gyroscope',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10675_performance',
        'a104174_gyroscope',
        100
  );

  // Add new attribute a57741_ambient_light_sensor
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57741_ambient_light_sensor', array(
    'label'                      => 'Ambient light sensor',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g10675_performance',
        'a57741_ambient_light_sensor',
        100
  );

  // Add new attribute a906_bundled_software
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a906_bundled_software', array(
    'label'                      => 'Bundled software',
    'input'                      => 'textarea',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Software distributed with another product such as a piece of computer hardware or other electronic device, or a group of software packages which are sold together. A software suite is an example of bundled software, as is software which is pre-installed on a new computer.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2933_software',
        'a906_bundled_software',
        100
  );

  // Add new attribute a57707_sim_card_support
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57707_sim_card_support', array(
    'label'                      => 'SIM card support',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g1711_networking',
        'a57707_sim_card_support',
        100
  );

  // Add new attribute a57280_builtin_speakers
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57280_builtin_speakers', array(
    'label'                      => 'Built-in speaker(s)',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The device contains speaker(s) to produce sound.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g110_audio',
        'a57280_builtin_speakers',
        100
  );

  // Add new attribute a58403_headphone_connectivity
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a58403_headphone_connectivity', array(
    'label'                      => 'Headphone connectivity',
    'input'                      => 'select',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'How headphones connect to a device e.g. wirelessly or via a cable and connector.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g36_ports__interfaces',
        'a58403_headphone_connectivity',
        100
  );

  // Add new attribute a238770_keyboard_included
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a238770_keyboard_included', array(
    'label'                      => 'Keyboard included',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g26489_packaging_content',
        'a238770_keyboard_included',
        100
  );

  // Add new attribute a131792_maximum_graphics_adapt
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a131792_maximum_graphics_adapt', array(
    'label'                      => 'Maximum graphics adapter memory',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'Graphics memory is determined by the (graphic) processor. Graphics memory is often shared with system memory so that it varies depending on the amount of system memory installed and system load.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g6417_other_features',
        'a131792_maximum_graphics_adapt',
        100
  );

  // Add new attribute a57699_graphics_adapter
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a57699_graphics_adapter', array(
    'label'                      => 'Graphics adapter',
    'input'                      => 'text',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  TRUE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g6417_other_features',
        'a57699_graphics_adapter',
        100
  );

  // Add new attribute a76655_64bit_computing
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a76655_64bit_computing', array(
    'label'                      => '64-bit computing',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'The use of processors that have datapath widths, integer size, and memory addresses widths of 64 bits (eight octets). In 2003 64-bit CPUs were introduced to the (previously 32-bit) mainstream personal computer arena in the form of the x86-64 and 64-bit PowerPC processor architectures and in 2012 even into the ARM architecture targeting smartphones and tablet computers, first sold on September 20, 2013 in the iPhone 5S powered by the ARMv8-A Apple A7 SoC.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g6417_other_features',
        'a76655_64bit_computing',
        100
  );

  // Add new attribute a94688_number_of_discrete_grap
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a94688_number_of_discrete_grap', array(
    'label'                      => 'Number of discrete graphics adapters supported',
    'input'                      => 'text',
    'type'                       => 'decimal',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a94688_number_of_discrete_grap',
        100
  );

  // Add new attribute a50681_keyboard_language
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a50681_keyboard_language', array(
    'label'                      => 'Keyboard language',
    'input'                      => 'multiselect',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g762_keyboard',
        'a50681_keyboard_language',
        100
  );

  // Add new attribute a904_certification
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a904_certification', array(
    'label'                      => 'Certification',
    'input'                      => 'textarea',
    'type'                       => 'text',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => '',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g2687_certificates',
        'a904_certification',
        100
  );

  // Add new attribute a130744_cuda
  $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'a130744_cuda', array(
    'label'                      => 'CUDA',
    'input'                      => 'boolean',
    'type'                       => 'varchar',
    'required'                   =>  FALSE,
    'comparable'                 =>  TRUE,
    'filterable'                 =>  TRUE,
    'filterable_in_search'       =>  FALSE,
    'used_for_promo_rules'       =>  FALSE,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'is_configurable'            => TRUE,
    'is_html_allowed_on_front'   => FALSE,
    'note'                       => 'CUDA (aka Compute Unified Device Architecture) is a parallel computing platform and programming model created by NVIDIA and implemented by the graphics processing units (GPUs) that they produce. CUDA gives program developers direct access to the virtual instruction set and memory of the parallel computational elements in CUDA GPUs.',
    'searchable'                 =>  TRUE,
    'sort_order'                 => '100',
    'unique'                     =>  FALSE,
    'used_for_sort_by'           =>  FALSE,
    'used_in_product_listing'    =>  TRUE,
    'user_defined'               =>  TRUE,
    'visible'                    =>  TRUE,
    'visible_on_front'           =>  TRUE,
    'visible_in_advanced_search' =>  TRUE,
    'wysiwyg_enabled'            =>  FALSE,
    'apply_to'                   =>  Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
    'option' => array("values" => array(
))
  ));

  $installer->addAttributeToSet(
    Mage_Catalog_Model_Product::ENTITY,
        'c151_Notebook',
        'g8602_graphics',
        'a130744_cuda',
        100
  );

  $installer->endSetup();
} catch (Exception $e) {
  Mage::logException($e);
  print_r("error");
}
  if ($installer instanceof Mage_Install_Model_Installer_Console) {
    if ($installer->getErrors()) {
        echo "\r\nFAILED\r\n";
        foreach ($installer->getErrors() as $error) {
            echo $error . "\r\n";
        }
    }
  }
exit(1);
?>