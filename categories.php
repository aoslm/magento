<?php
if (version_compare(phpversion(), '5.2.0', '<')===true) {
    die('ERROR: Whoops, it looks like you have an invalid PHP version. Magento supports PHP 5.2.0 or newer.');
}

set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());
require 'app/bootstrap.php';
require 'app/Mage.php';

try {
    $app = Mage::app('default');
    $installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
    //$installer->startSetup();
// Attribute Group g6417_other_features
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g6417_other_features', 100);

// Attribute Group g107_hard_drive
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g107_hard_drive', 100);

// Attribute Group g32_optical_drive
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g32_optical_drive', 100);

// Attribute Group g35_processor
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g35_processor', 100);

// Attribute Group g2687_certificates
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g2687_certificates', 100);

// Attribute Group g2933_software
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g2933_software', 100);

// Attribute Group g7946_battery
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g7946_battery', 100);

// Attribute Group g33_display
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g33_display', 100);

// Attribute Group g1711_networking
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g1711_networking', 100);

// Attribute Group g110_audio
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g110_audio', 100);

// Attribute Group g4124_storage
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g4124_storage', 100);

// Attribute Group g762_keyboard
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g762_keyboard', 100);

// Attribute Group g36_ports__interfaces
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g36_ports__interfaces', 100);

// Attribute Group g106_memory
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g106_memory', 100);

// Attribute Group g2684_video
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g2684_video', 100);

// Attribute Group g1403_operational_conditions
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g1403_operational_conditions', 100);

// Attribute Group g109_power
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g109_power', 100);

// Attribute Group g108_weight__dimensions
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g108_weight__dimensions', 100);

// Attribute Group g8602_graphics
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g8602_graphics', 100);

// Attribute Group g10674_design
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g10674_design', 100);

// Attribute Group g6416_packaging_data
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g6416_packaging_data', 100);

// Attribute Group g269_technical_details
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g269_technical_details', 100);

// Attribute Group g2686_security
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g2686_security', 100);

// Attribute Group g10675_performance
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g10675_performance', 100);

// Attribute Group g26489_packaging_content
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g26489_packaging_content', 100);

// Attribute Group g3024_camera
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g3024_camera', 100);

// Attribute Group g10385_processor_special_featu
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g10385_processor_special_featu', 100);

// Attribute Group g12758_docking_station
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g12758_docking_station', 100);

// Attribute Group g20050_features
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g20050_features', 100);

// Attribute Group g26477_brandspecific_features
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g26477_brandspecific_features', 100);

// Attribute Group g26515_logistics_data
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g26515_logistics_data', 100);

// Attribute Group g26889_additional_design_featu
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g26889_additional_design_featu', 100);

// Attribute Group g26890_test_and_awards
    $entityTypeId = $installer->getEntityTypeId('catalog_product');
    $attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
    $installer->addAttributeGroup($entityTypeId, $attributeSetId, 'g26890_test_and_awards', 100);

  $installer->endSetup();
} catch (Exception $e) {
  Mage::logException($e);
  print_r("error");
}
  if ($installer instanceof Mage_Install_Model_Installer_Console) {
    if ($installer->getErrors()) {
        echo "\r\nFAILED\r\n";
        foreach ($installer->getErrors() as $error) {
            echo $error . "\r\n";
        }
    }
  }
exit(1);
?>